
Folder contains:
1). Netbeans(IDE) Project, contains source code and libraries used.
2). JAR folder contains executable jar, ready to execute and other jar files needed.
3). Sample Image folder contains sample TIFF image for testing.
  
Bump Mapping Application is written in java, with java platform as JDK(1.7).
It can be executed from command line as:
java -jar <path-of-"CG-Project.jar">.

Steps to view the demo:
1). Choose the input file.
2). File -> Open-> <select-tiff-file-from-file-chooser> -> Open.
3). Click on "Bump Images" Button.
4). Select the output folder where processed image will be stored. Click Open. 
4). Processing can be checked on the cmd window.
5). Application terminates itself when the processing is completed.
6). Output is generated as <file-name>.jpg image.



